import 'package:pokedex/pokemon.dart';

void main() {
  var charizard = Pokemon(altura: 2, nome: "Charizard", peso: 5);
  var bulbasaur = Pokemon(altura: 3, nome: "Bulbasaur", peso: 3);

  print(charizard.nome);
  print(bulbasaur.nome);
  print(bulbasaur.atacar());
}
