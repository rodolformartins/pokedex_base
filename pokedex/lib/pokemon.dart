class Pokemon {
  //Atributos
  late String nome;
  late int altura;
  late int peso;

  //metódo construtor
  Pokemon({required this.nome, required this.altura, required this.peso});

  //Metodos
  atacar() {
    print("Seu pokémon atacou!");
  }

  defender() {
    print("Seu pokémon defendeu!");
  }
}
